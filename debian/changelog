libcatalyst-actionrole-acl-perl (0.07-2) UNRELEASED; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jotam Jr. Trejo from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add missing build dependency on libmodule-install-perl.

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 06 Jan 2013 21:57:49 +0100

libcatalyst-actionrole-acl-perl (0.07-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Wed, 06 Jan 2021 16:37:53 +0100

libcatalyst-actionrole-acl-perl (0.07-1) unstable; urgency=low

  * Imported Upstream version 0.07
  * debian/control:
    + added versioned dependency against libcatalyst-perl >= 5.90013
      since the new changes provided by upstream rely on changes in
      libcatalyst-perl 5.90013 and later versions
    + removed libcatalyst-controller-actionrole-perl from the dependencys
      because Catalyst::Controller::ActionRole is provided by
      libcatalyst-perl since version 5.90013
  * Added myself to Uploaders and Copyright
  * This new version use Catalyst::Controller instead of
    Catalyst::Controller::ActionRole fixing FTBFS (Closes: #680819)

 -- Jotam Jr. Trejo <jotamjr@debian.org.sv>  Fri, 13 Jul 2012 11:29:42 -0600

libcatalyst-actionrole-acl-perl (0.06-1) unstable; urgency=low

  * Initial release (closes: #667822).

 -- gregor herrmann <gregoa@debian.org>  Fri, 06 Apr 2012 22:36:42 +0200
